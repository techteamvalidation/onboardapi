import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

// Run on local with nestjs

// admin.initializeApp({
//     credential: admin.credential.cert({
//         projectId: 'onboard-web-app',
//         clientEmail: 'firebase-adminsdk-i7sy6@onboard-web-app.iam.gserviceaccount.com',
//         privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQDDEZ8Xc39eIE1m\nD1bMHPxyE7I1zmyUQkAcZKid6IvPMfTBCU8SToQ13ewWP69+nNVzv8kev/y7q6b5\nD9lf4m8ex1Alvbk5uRDMlH7OeRE4kwdz7j6LIw9XBLvaqiDRLiL1RKmNdjT/et8j\nXoqr2PewZQhRo00u2vw0Sszxs9/6lMH+IlRDxz51gmW9CRXD523hkgsyxVz0uFMJ\n6vE6jCLbSRCgUn76uA0/Ylse+h6sc9hG9yPfRDnuNwW3E52pPBJ1GTkWgpC+8b+B\njoNxfNBig2vthmIi25J1xm94eFJhhNgGivbm0+zqKkWYS3sDxhrelzHLE25YYPuP\nENtXZgcDAgMBAAECgf9sR1vGU1y7zyVOyd+Ywr/hs5Bzouavmcf8iyuD3iHxpfCf\nQ3fb34UAo0LFMRf3fsg4ZFcX9aFN01zb6gqK7UAwzvVfbVM8TlALc9G0oz2w1FVo\n/hE2PjK7zAxF/zA4NQgEpxHf0BmbDBYLfuVGIs2cFfWb3qCqgm2w9Q94shE43qMx\nm63R6ovhFouuI5hMH7/THsWcryaaFLLONFYDdNSlkdP3Css04boWMmNKfp4XdHly\nZfA7fQLCTKBaLCr+IEvF3xjdlyNMVbOS0EsE17lhyVAV84lOtwlhCh42r8PR+3DQ\nW6UJLP5hqK42r9BXLm5fWs3Is9LA7Ap7EWEBbTECgYEA67GE6wSXuDg/cw3tIeQo\ncvyJ9GNUVUn3lW5nGNqF2LuChvxPd47FpHNAVBk2+ik3VJ+yEYkB2BIFLdoyFO3l\nu6BDy1vpOJAHsv9xqKNWNiCU6f0357KQjBvOSoKhfmLOeoiTzo2S8PiNh1LmJbKj\npS+yBwwuoLzpZG7WDEcw5jcCgYEA0+AU3Ry3UU0p4pxSkJre9CaJdmdJaHrNpESG\nOSwEH0GbRwmjhXL/mgypt9jYAHFGfQo2kOCrT06cFcnLz26ujirR68zyNoZWR24+\n6IC34SykFiumdpmw3D0xUCgIekyt52AGl2EbVIHxlHchYWqYbnMLsoiJZwXwMzl8\nYM67v5UCgYAVnS/tRXyMXkwjRvGqG/xBnzsfeERxldjokTlPJCsGFxv/OQAonKUv\nUYpiq/VIt6LlVcfEp+XNZMmozMuu86oPrMDK02Dt2Pd7yMUpi+EKEa7WTmxkCRIM\nZzhhyNz6dCEvR3sWhBEnnAOOl2GXr2wmZ9tRniJghzJ4DVn/wj+PvQKBgQCeO0tZ\nloiRfz84reY7eG3piJ5AfwKjtJK/iy1ypRyu+yAr/VbR4wBg2/QkCR3HttEdsPLv\nX9qQb/TBkONVFyPlqLT9lSGO9uSYlA0HNpoc4Gnq5M585GrhEBIz+rHaJEn78gr6\nItt2UVVJu26Cgkk0s1aZDr8O4bBxkQ0Cf2JXwQKBgDnjqZznz9/ONvum97aP3rY0\nhH8yT8eL+hKxxQamjVvB+qswmvj64+qR6a7O4xXYdyuDD9NpU67/T7TzCZIS3Ghi\nyzQ7PxgqW0SfTgxc0M+sLq8HP6dCWiMhm1wN+qCPahDXDln/QWCDxwFpcUGtEcQw\n7bi/7oPsJwMiICgJhK+k\n-----END PRIVATE KEY-----\n',
//     }),
//     databaseURL: 'https://onboard-web-app.firebaseio.com',
// });

// Deploy to cloud functions

admin.initializeApp(functions.config().firebase);

const db = admin.firestore();
const auth = admin.auth();

export {
    db,
    auth,
};
