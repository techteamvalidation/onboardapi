import * as functions from 'firebase-functions';
import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { ValidationPipe } from '@nestjs/common';

import { AppModule } from './app.module';
const server: express.Express = express();

async function bootstrap(expressInstance) {
  const app = await NestFactory.create(AppModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrap(server);

exports.api = functions.https.onRequest(server);
