import { Controller, Get } from '@nestjs/common';

@Controller('app')
export class AppController {
    @Get('welcome')
    root(): string {
        return 'Welcome!';
    }
}
